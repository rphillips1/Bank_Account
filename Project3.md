### *CS-140 BL --- Summer I 2016*

# Project 3 - Bank Accounts

## Objectives
In this project, you will modify an existing `BankAccount` class to add new funtionality. At the end of this project, you should be comfortable adding new instance variables and methods, using the `if` statement, reading API documentation, calling methods of library classes, writing `javadoc` comments, and compiling and testing your code.

## Specifications
In this project you will create two classes: * `BankAccount`* `BankAccountTester`
Study the `BankAccount` given in your book (Chapter 3).  You need to use that existing class and add/modify functionality as described below:
In this project, you will enhance the `BankAccount` class and see how abstraction and encapsulation enable evolutionary changes to software. 

Begin with a simple enhancement: charging a fee for every deposit and withdrawal. Supply a mechanism for setting the fee and modify the deposit and withdraw methods so that the fee is levied. Test your class and check that the fee is computed correctly. 

Now make a more complex change. The bank will allow a fixed number of free transactions (deposits or withdrawals) every month, and charge for transactions exceeding the free allotment. The charge is not levied immediately but at the end of the month. Supply a new method `deductMonthlyCharge` to the `BankAccount` class that deducts the monthly charge and resets the transaction count. (Hint: Use `Math.max`(actual transaction count, free transaction count) in your computation.) 

Produce a test program (`BankAccountTester`) that verifies that the fees are calculated correctly over several months.To realize the above functionality, you need to figure what additional pieces of information each `BankAccount` object must have.  For every `BankAccount` object, you need to keep track of(in addition to the balance) the number of free transactions allowed on that account, total number of transactions made so far and the transaction fee.
I have attached the `doc` folder that has the documentation for the `BankAccount` class.
In your `BankAccountTester` class within the main method do the following:
1.	Create a `BankAccount` object with an initial balance of 2000 with 3 free transactions.  2.	Set a transaction fee on this object (you can choose any fee you want to impose)3.	Do multiple withdrawals and deposits into this account (at least 6 transactions)4.	Deduct the monthly charge by calling the appropriate method on your `BankAccount` object5.	Figure out what the balance should be (do it manually on paper) and print your expected value and the balance (returned by the `getBalance()` method)6.	Repeat steps 1-4 two more times and check your balance to make sure that your program works.
   **Important Note:** Your `BankAccount` class must have appropriate `javadoc` comments for every method.  Both classes should have the `javadoc` comments giving a short description of your file, your name and version number.
 
### This is an Individual Assignment - No Partners
As this is a Project (and not a Lab) you will be working on your own, not with a partner. You should not be sharing your code with anyone else, other than the instructor.  

You will need to fork your own private Project3 repository on GitLab for this project. The only person who should have any access to your repository is your instructor.  

You can ask questions on Piazza about setting up your repository on GitLab, about using Git to send code to the instructor, and general questions about how to write your code. However you should not be posting sections of code and asking others to find your errors. 

### Deliverables
Be sure that you have your name and an explanation of what your program does in the Javadoc comments. Be sure that you have indented consistently.  
 
The instructor will pull your Project3 from your GitLab repository to grade it. Make sure:
 
1. You have pushed all changes to your shared repository. (I can’t access local changes on your computer.)  
2. You have added your instructor as Master to your shared GitLab repository.  

### Due Date/Time
Your project must be pushed to GitLab by Tuesday, 14 June 2016 at 11:59pm. 
	
##Copyright and License
####&copy;2016 Karl R. Wurst and Aparna Mahadev, Worcester State University

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit <a href="http://creativecommons.org/licenses/by-sa/4.0/" target="_blank">http://creativecommons.org/licenses/by-sa/4.0/</a> or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.