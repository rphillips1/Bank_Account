import java.util.Scanner;

/**
 * Write a description of class BankAccountTester here Project 3.
 * CIS 140 - Summer I
 * 
 * @author Rick W. Phillips 
 * @version June 10th, 2016
 */
public class BankAccountTester
{
    /**
     * main method for testing the BankAccunt class
     * 
     * @param String arguments for the main method
     */
    public static void main(String[] args) {
        double amount = 0.0;
        int option = 0;
        
        // Call method to display welcome message and wait for appropriate results
        do {
            amount = displayWelcome(amount);
        } while (amount <= 0.0);
        
        // Create the account object. I created the method as I 
        // do not want to print to the screen in the main method
        // except on exit
        BankAccount account = createAccount(amount);
        // reset the amount variable for transactional use
        amount = 0.0;
        // We have 4 available options
        // 1 deposit, 2 withdrawal, 3 end of month, and 4 done
        // We operate on 1, 2 and 3 and end with 4
        // dispalyGetOptions sets options to 0 with bad input data
        do {
            // Wait for correct input data not 0
            option = displayGetOptions(option);
            if(option != 0) {
                switch(option) {
                    // Begin a deposit operation
                    case 1: 
                        do {
                            amount = displayGetAmount("Deposit");
                        } while(amount <= 0.0);
                        // Perform operation and display fee notice if our model tells us we have exceeded
                        if(account.deposit(amount) == true) {
                            displayFeeNotice();
                        }
                        // Display the results
                        displayTransaction("Deposit", amount);
                    break;
                    // Begin a withdraw operation
                    case 2: 
                        do {
                            amount = displayGetAmount("Withdraw");
                        } while(amount <= 0.0);
                        // Perform operation and display fee notice if our model tells us we have exceeded
                         if(account.withdraw(amount) == true) {
                            displayFeeNotice();
                        }
                        // Display the results
                        displayTransaction("Withdrawal", amount);
                    break;
                    case 3:
                        // Invoke the deductMonthlyCharges and get the amount reconciled as a double
                        amount = account.deductMonthlyCharge();
                        // Display the charges and status reset to user
                        displayMonthlyCharges(amount);
                    break;
                }
            }
            // Display the post transaction status if not exiting and reset amount variable
            if(option != 4){
                System.out.print(account);
            }
            amount = 0.0;
        } while (option != 4); // Exit selection
        
        // Display exit program text
        System.out.print("\n\n\n\n*** Thank for using the Bank Account Class by Rick W. Phillips ***\n\n\n\n\n\n\n");
    }
    
    /**
     * Create a new BankAccount object with a given amount and display creation text
     * 
     * @param double amount as initial balance
     * @return BankAccount object for testing
     */
    public static BankAccount createAccount(double amount) {
        BankAccount account = new BankAccount(amount);
        // Display account creation text and account status
        System.out.println("\n\n\n*****************************");
        System.out.println("*** BANK ACCOUNT CREATED  ***");
        System.out.print(account);
        return account;
    }
    
    /**
     * Display the bank account creation wizard to retrieve correct input
     * 
     * @param amount the amount of the intial balance
     * @return the amount to be used as the intial balance
     */
    public static double displayWelcome(double amount) {
        String in = "";
        Scanner keyboard = new Scanner(System.in);
        
        // Display the welcome text
        System.out.println("\n****************************************************");
        System.out.println("*** Welcome to the Bank Account Creation Wizards ***");
        System.out.print("*** Please enter an intitial balance: ");
        in = keyboard.next();
        
        // Validate correct input or display an error message
        if (in.matches("[0-9]+[.]*[0-9]*")) {
            amount = Double.parseDouble(in);
        }
        else {
            System.out.println("\nERROR: " + in + " is not a correct initial balance amount.");
            System.out.println("\n*** Please try again ****\n");
            // Set amount to bad data status
            amount = 0.0;
        }
        
        return amount;
    }
    
    /**
     * display and retrieve correct transaction option input
     * 
     * @param int option to be manipulated
     * @return int option after input selection
     */
    public static int displayGetOptions(int option) {
        String in = "";
        Scanner keyboard = new Scanner(System.in);
        
        // Display options to the user
        System.out.println("\n*** Select an option number\n*** 1 Deposit\n*** 2 Withdraw\n*** 3 End Month\n*** 4 Exit");
        System.out.print("Enter a selection: ");
        
        in = keyboard.next();
        // Validate input data and display error if needed
        if(in.matches("[1-4]")) {
            option = Integer.parseInt(in);
        }
        else {
            System.out.println("\nERROR: " + in + " is not a correct selection.");
            System.out.println("\n*** Please try again ****\n");
            // Set options to bad data status
            option = 0;
        }
        
        return option;
    }
    
    /**
     * prompt user to enter a correct amount for a transaction
     * 
     * @param String the type of transaction 
     * @return double a correct amount for the transaction of 0.0 if to try again
     */
    public static double displayGetAmount(String type) {
        double amount = 0.0;
        String in = "";
        Scanner keyboard = new Scanner(System.in);
        
        System.out.print("\n*****************************\n");
        System.out.print("******    " + type.toUpperCase() + "    *******");
        System.out.print("\n*****************************\n");
        System.out.print("Enter a " + type + " amount: ");
        
        in = keyboard.next();
        // Validate input data and display error if needed
        if (in.matches("[0-9]+[.]*[0-9]*")) {
            amount = Double.parseDouble(in);
        }
        else {
            System.out.println("\nERROR: " + in + " is not a correct " + type + " amount.");
            System.out.println("\n*** Please try again ****\n");
            // Set amount to bad data status
            amount = 0.0;
        }
        
        return amount;
    }
    
    /**
     * display the fee notice to the user for this transaction
     */
    public static void displayFeeNotice() {
        System.out.println("*****************************");
        System.out.println("* NOTICE OF TRANSACTIUON FEE *");
        System.out.println("*****************************");
        System.out.println("You have exceeded the allowed");
        System.out.println("tranactions this month. A $1.00 Fee");
        System.out.println("will be charged for this transcation");
        System.out.println("at the end of this month");
    };
    
    /**
     * display the results of a transaction
     * 
     * @param String the type of transaction
     * @param double the amount of the transaction
     */
    public static void displayTransaction(String type, double amount) {
        System.out.println("*****************************");
        System.out.println("YOUR " + type.toUpperCase() + " OF $" + amount + " WAS SUCCESSFUL!");
    }
    
    /**
     * display the monthly charges accrued for this month
     * 
     * @param double the amount of this months charges to display
     */
    public static void displayMonthlyCharges(double amount) {
        System.out.println("*****************************");
        System.out.println("***** END OF MONTH FEES *****");
        System.out.println("*****************************");
        System.out.println("You exceeded your limit " + (int)amount + " times.");
        System.out.println("Your account has been charged $" + (int)amount + ".00");
        System.out.println("Your transaction count is reset");
        System.out.println("*****************************");
    }
}
