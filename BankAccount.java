
/**
 * Class to implement the Bank Account Class per Project 2 CIS140 - Summer 1. A Bank account has a 
 * balance that can be changed by deposits and withdrawals.
 * 
 * @author Richard Phillips
 * @version June 9th, 2016 v0.1
 */
public class BankAccount
{
    private double balance;
    private int maxTransactions;
    private int transactionCount;

    /**
     * Constructs a bank account with zero balance.
     */
    public BankAccount()
    {
        balance = 0.0;
        maxTransactions = 3;
        transactionCount = 0;
    }
    
    /**
     * Constructs a bank account with a given balance.
     * 
     * @param initialBalance the initial balance
     */
    public BankAccount(double initialBalance)
    {
        balance = initialBalance;
        maxTransactions = 3;
        transactionCount = 0;
    }

    /**
     * Deposit money into the bank account.
     * 
     * @param amount the amount to deposit 
     * @return boolean if max transactions has been exceeded
     */
    public boolean deposit(double amount) {
        transactionCount++;
        //amount -= transactionCount > maxTransactions ? 1.0 : 0.0;
        balance += amount;
        return transactionCount > maxTransactions;
    }
    
    /**
     * Withdraw money from the bank account.
     * 
     * @param amount the amount to withdraw   
     * @return boolean if max transactions has been exceeded
     */
    public boolean withdraw(double amount) {
        transactionCount++;
        //amount += transactionCount > maxTransactions ? 1.0 : 0.0;
        balance -= amount;
        return transactionCount > maxTransactions;
    }
    
    /**
     * Get the current balance of the bank account.
     * 
     * @return the current balance   
     */
    public double getBalance() {
        return balance;
    }
    
    /**
     * Reconcile any transaction fees and reset the count
     * 
     * @return double the amount charges for the fees this month
     */
    public double deductMonthlyCharge() {
        double amount = 0.0;
        
        if(this.transactionCount - this.maxTransactions > 0) {
            amount = this.transactionCount - this.maxTransactions;
            this.withdraw(amount);
        }
        this.transactionCount = 0;
        return amount;
    }
    
    /**
     * Get the current status of the bank account
     * 
     * @return String the current status   
     */
    public String toString() {
        String text = "*****************************\n";
        text += "*** Account Balance: $" + this.balance +"\n";
        text += "*****************************\n";
        text += "*** You have used " + this.transactionCount + " of " + this.maxTransactions;
        text += "\n*** transactions this month.\n";
        text += "*****************************";
        
        return text;
    }
    
}
